var quiz = [
    {
        question: "wat is dit",
        answers: [{answer: "test", points: 0}, {answer: "test1", points: 1}, {answer: "test2", points: 2}]
    },
    {
        question: "wat is dit1",
        answers: [{answer: "test", points: 0}, {answer: "test1", points: 1}, {answer: "test2", points: 2}]
    },
    {
        question: "wat is dit2",
        answers: [{answer: "test", points: 2}, {answer: "test1", points: 1}, {answer: "test2", points: 0}]
    }

];

var index = 0;
var playerScore = 0;
var maxScore = 0;

function start() {
    score = 0;
    index = 0;
    showQuestion(); 
}

function showStart() {
    $(".quiz").empty();
    var title = $("<h2>Quiz about content creating</h2>");
    var btn = $("<button class=\"btn btn-primary\" onclick=\"start()\">Start the quiz</button>");
    $(".quiz").append(title, btn);
}

function showQuestion() {
    $(".quiz").empty();
    var question = quiz[index]["question"];
    console.log(question, index, playerScore);
    $(".quiz").append($("<h3>" + question + "</h3>"));

    var tMax = 0;

    for (var i = 0; i < quiz[index]["answers"].length; i++) {
        var a = quiz[index]["answers"][i];
        var text = a["answer"];
        var score = a["points"];
        var answer = $("<p><label><input type=\"radio\" id=\"answer\" name=\"answer\" value=\"" + score + "\"><span>" + text + "</span></label></p>");
        $(".quiz").append(answer);
        tMax = tMax < score ? score : tMax;
    }

    maxScore += tMax;
    
    var button = $("<br><button class=\"btn btn-primary\" onclick=\"answerQuestion()\">Answer Question</button>");
    $(".quiz").append(button);
}

function answerQuestion() {
    var value = $('input[name=answer]:checked').val();
    if (!isNaN(value)) {
        playerScore += parseInt(value);
        if (index < quiz.length - 1) {
            index++;
            showQuestion();
        } else {
            showEnd();
        }
    }
}

function showEnd() {
    $(".quiz").empty();
    console.log(playerScore);

    var end = $("<h2>Results</h2>");
    $(".quiz").append(end);

    var points = $("<p>Score: <b>" + playerScore + "</b> out of <b>" + maxScore + "</b></p>");
    $(".quiz").append(points);

}

showStart();
